# dwmstatus

## About
This repository is a fork of the suckless [dwmstatus](https://git.suckless.org/dwmstatus) project. 


## Modifications:
* The timezone can be changed via a `config.h`
* The network up/down speeds are displayed
* The memory usage is shown in percentage
