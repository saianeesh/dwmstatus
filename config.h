/* 
 * Timezone setting for dwmstatus
 */

#if !defined(TIMEZONE)
    #define TIMEZONE "Australia/Brisbane"
#endif
